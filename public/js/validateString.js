var url = window.location.origin;
var token = $('meta[name="csrf-token"]').attr('content');
$('#btnValidate').on('click',function () {

    var stringInput = $('#string').val();
    if(stringInput==''){
        Swal.fire({
            type: 'error',
            title: 'Place Enter a String',
          })
    }else{
        $.ajax({
            url: url + '/validateString',
            type: 'POST',
            data: {
                stringInput:stringInput,
                _token: token
            },
            success: function (data){
                if(data['status']=="200"){
                    valid = 'valid'; 

                }else{
                    valid = 'invalid';
                } 
                var htmlTags = 
                        '<tr>'+
                                '<td>' + data['dateTime'] + '</td>'+
                                '<td>' + data['input'] + '</td>'+
                                '<td>' + data['output'] + '</td>'+
                                '<td>' + valid + '</td>'+
                        '</tr>';
                $('.table tbody').append(htmlTags);
                $('input[type="text"]').val('');
                $( "#string" ).focus();
            }
        });
    }
});
