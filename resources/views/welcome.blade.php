<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Excercise</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">


        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
   </head>
<body>
   <div class="container ">
   <div class="row">   
       <div class="col-md-6 col-md-offset-4">
           <div class="panel panel-default">
                <h1><p class="text-center">EXCERCISE</p></h1>
               <div class="panel-heading"> <label for="exampleInputEmail1">Validate String</label></div>
               <div class="panel-body">
                   <form method="POST" action="">
                        {{ csrf_field() }}
   
                        <div class="form-group">
                            <label for="string">Input:</label>
                            <input type="text" name="string" id="string" class="form-control" placeholder="Enter string">
                        </div>
                      
 

 
               </div>
               <div class="panel-footer">
                       <button type="button" id="btnValidate" class="btn btn-primary">{{__('Submit')}}</button>
               </div>
               </form>               
           </div>
 
 
 
 
       </div>
   </div> 
  
   <div class="row">
       <div class="col-md-6 col-md-offset-4">
               <h2>History</h2>
                          
               <table class="table" border="2">
                 <thead>
                   <tr>
                     <th>DateTime</th>
                     <th>Input</th>
                     <th>Output</th>
                     <th>Validate</th>
                   </tr>
                 </thead>
                 <tbody>


                </tbody>
               </table>
       </div>
   </div>
   </div>
   <script src="https://code.jquery.com/jquery-3.4.1.js"
       integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
       integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
       crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.14.0/dist/sweetalert2.all.min.js"></script>

        <script src="{{ asset('js/validateString.js') }}"></script>

</body>
 
<footer>
    <script type="text/javascript">
        $(document).ready(function() {
            $("form").keypress(function(e) {
                if (e.which == 13) {
                    return false;
                }
            });
        });
    </script>
</footer>
</html>
