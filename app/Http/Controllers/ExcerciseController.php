<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;


class ExcerciseController extends Controller
{
    public function __construct(){

    }
    public function index(){
        return view('inputString');
    }

    public function validateString(Request $request){
        
        $data = $request->stringInput;

        $stringLower = strtolower($data);
     
        
        $letters = [];
        
        
        $result = $this->countCharacters($stringLower);


        $date = Carbon::now();
        
        $inputString="";
        foreach($result as $key => $value)
        {
                $inputString .= $key;
        }


        $maximum = max($result);
        $minimum = min($result);

        $amax =array_count_values($result)[$maximum];
        $amin = array_count_values($result)[$minimum];
        
        $dataOut = array(
            'output'    => $inputString,
            'input'     => $data,
            'dateTime'  => $date->format('d-m-Y g:i A'),
        );

        if ($maximum == $minimum) {
            $dataOut['status'] = '200';
            return $dataOut;
        } else if (($amin == 1 && ($minimum == 1 || $minimum - $maximum == 1)) || ($amax == 1 && ($maximum == 1 || $maximum - $minimum == 1))) {
            $dataOut['status'] = '200';
            return $dataOut;
        } else {
            $dataOut['status'] = '400';
            return $dataOut;
        }

    }

    private function countCharacters($stringLower){
        $arrayLeters= array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
        foreach ($arrayLeters as $letter) {
            if (substr_count($stringLower, $letter)) {
                $letters[$letter] = substr_count($stringLower, $letter);
                
            }
        }
        return $letters;
    }
    
}
